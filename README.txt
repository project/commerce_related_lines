
-- SUMMARY --

This module provides a line item type with pre-built fields relating to a
product line item. This allows one to add a product to the cart and assign
discounts and fees to the related line item that don't get multiplied by the
quantity of the product line item.

Pricing rules in this module allow one to respond to the price calculation event
and add, multiple, divide, subtract and set values into a related line item.
They work on either the product or cart level. If an existing related line item
already exists, it uses it. Otherwise it creates a new one. This way only a
single related line item per price type is created.

This data from this module is fully usable in Views to modify the cart or build
custom admin screens.

           ====================================================

Use case #1:
Taxes are calculated by a 3rd party service on the entire price of a product
including multiplication by the quantity of said product). If the tax amount
were applied against the product line item, it would need to be divided by the
product line item quantity before being applied. This leads to rounding issues.

Example:
Tax                  SKU             Quantity        SKU Price       Tax Total
7.5%         ISBN 9781602523692       3x               $10.97          $2.47

$2.47 doesn't divide nicely into 3 and isn't easily applied against the product
line item.

           ====================================================

Use case #2:
A complicated delivery charge should be calculated, displayed and related per
product line item, not in the order subtotals.

Example:
  Fees                  SKU             Quantity       SKU Price      Fee Total
Delivery Charges        Gravel             3x            $17.87         $13

Complicated math goes into determining the delivery charge should be $13. This
amount doesn't doesn't divide nicely into 3 and isn't easily applied against the
product line item.

           ====================================================

Use case #3:
Discounts are calculated by a 3rd party service. These need to be applied on the
entire cart, not a single product line.

Example:
Cart Discount         SKU        Quantity        SKU Price     Discount Total
Free Freight          n/a          n/a              n/a            -$7.00

$7.00 might or might not divide easily into an existing product line item. Nor
do you need to create a custom line item or dummy product. All you need is the
discount price component, which comes by default with commerce_price. Or it is
very simple to create a new one if one needs to distinguish. And one can
implement hook_commerce_related_lines_title_alter() to adjust the title too.

           ====================================================

For a full description of the module, visit the project page:
  http://drupal.org/sandbox/lucashedding/2087563

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/2087563


-- REQUIREMENTS --

* commerce_product_reference

-- INSTALLATION --


* Install library as usual, for futher information see
  https://drupal.org/node/1440066.

-- API --

    function example_commerce_related_lines_title_alter(&$title, $line_item) {
      // If this has a related line item, then append it's title.
      if (!empty($line_item->commerce_product_line_item_ref)) {
        $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);
        $product_title = $line_item_wrapper->commerce_product_line_item_ref->commerce_product->title->value();
        $title .= ': ' . check_plain($product_title);
      }
    }

    function example_commerce_related_lines_quantity_alter(&$quantity, $line_item) {
      $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);
      // For discounts, change quantity to n/a.
      if ($line_item_wrapper->commerce_related_lines_type->value() == 'discount') {
        $quantity = t('n/a');
      }
    }

-- CONTACT --

Current maintainer:
* Lucas Hedding (heddn) - http://drupal.org/user/1463982
