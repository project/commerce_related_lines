<?php

/**
 * @file
 * Rules for Commerce Related Lines.
 */

/**
 * Implements hook_rules_action_info().
 *
 * Copies from commerce_line_item_rules_action_info().
 */
function commerce_related_lines_rules_action_info() {
  // Prepare an array of arithmetical actions for altering prices.
  $action_types = array(
    'commerce_related_lines_price_add' => array(
      'label' => t('Add an amount to a related line item'),
      'amount description' => t('Specify a numeric amount to add to the price.'),
    ),
    'commerce_related_lines_price_subtract' => array(
      'label' => t('Subtract an amount from a related line item'),
      'amount description' => t('Specify a numeric amount to subtract from the price.'),
    ),
    'commerce_related_lines_price_multiply' => array(
      'label' => t('Multiply a related line item by some amount'),
      'amount description' => t('Specify the numeric amount by which to multiply the price.'),
    ),
    'commerce_related_lines_price_divide' => array(
      'label' => t('Divide a related line item by some amount'),
      'amount description' => t('Specify a numeric amount by which to divide the price.'),
    ),
    'commerce_related_lines_price_amount' => array(
      'label' => t('Set a related line item to a specific amount'),
      'amount description' => t('Specify the numeric amount to set the price to.'),
    ),
  );

  // Define the action using a common set of parameters.
  foreach ($action_types as $key => $value) {
    $actions[$key] = array(
      'label' => $value['label'],
      'parameter' => array(
        'order' => array(
          'type' => 'commerce_order',
          'label' => t('Commerce Order'),
        ),
        'product_line_item' => array(
          'type' => 'commerce_line_item',
          'description' => t('Optionally pair this line item with a product line item to provide a purchase price adjustment reguardless of quantity.'),
          'bundle' => 'commerce_product',
          'label' => t('Product Line Item'),
          'optional' => TRUE,
          'allow null' => TRUE,
        ),
        'amount' => array(
          'type' => 'decimal',
          'label' => t('Price adjustment'),
          'description' => t('.86 is $0.86 (price markup). and -5.00 is -$5.00 (price discount).'),
        ),
        'component_name' => array(
          'type' => 'text',
          'label' => t('Available price components'),
          'options list' => 'commerce_price_component_titles',
        ),
        'round_mode' => array(
          'type' => 'integer',
          'label' => t('Price rounding mode'),
          'description' => t('Round the resulting price amount after performing this operation.'),
          'options list' => 'commerce_round_mode_options_list',
          'default value' => COMMERCE_ROUND_HALF_UP,
        ),
      ),
      'group' => t('Commerce Related Lines'),
    );
  }

  return $actions;
}

/**
 * Rules action: add an amount to the related line item price.
 */
function commerce_related_lines_price_add($order, $product_line_item, $amount, $component_name, $round_mode) {
  // See if an existing related line item already exists.
  $line_item = _commerce_related_lines_retrieve_line($order, $product_line_item, $component_name);

  // Set type and (optional) product reference.
  _commerce_related_lines_title($line_item, $product_line_item, $component_name);

  // Now add the amount.
  module_load_include('inc', 'commerce_line_item', 'commerce_line_item.rules');
  commerce_line_item_unit_price_add($line_item, $amount, $component_name, $round_mode);

  // Now merge it into the order.
  _commerce_related_lines_merge_line($order, $line_item);
}

/**
 * Rules action: subtract an amount from the related line item price.
 */
function commerce_related_lines_price_subtract($order, $product_line_item, $amount, $component_name, $round_mode) {
  // See if an existing related line item already exists.
  $line_item = _commerce_related_lines_retrieve_line($order, $product_line_item, $component_name);

  // Set type and (optional) product reference.
  _commerce_related_lines_title($line_item, $product_line_item, $component_name);

  // Now subtract the amount.
  module_load_include('inc', 'commerce_line_item', 'commerce_line_item.rules');
  commerce_line_item_unit_price_subtract($line_item, $amount, $component_name, $round_mode);

  // Now merge it into the order.
  _commerce_related_lines_merge_line($order, $line_item);
}

/**
 * Rules action: multiply the related line item price.
 */
function commerce_related_lines_price_multiply($order, $product_line_item, $amount, $component_name, $round_mode) {
  // See if an existing related line item already exists.
  $line_item = _commerce_related_lines_retrieve_line($order, $product_line_item, $component_name);

  // Set type and (optional) product reference.
  _commerce_related_lines_title($line_item, $product_line_item, $component_name);

  // Now multiply the amount.
  module_load_include('inc', 'commerce_line_item', 'commerce_line_item.rules');
  commerce_line_item_unit_price_multiply($line_item, $amount, $component_name, $round_mode);

  // Now merge it into the order.
  _commerce_related_lines_merge_line($order, $line_item);
}

/**
 * Rules action: divide the related line item price.
 */
function commerce_related_lines_price_divide($order, $product_line_item, $amount, $component_name, $round_mode) {
  // See if an existing related line item already exists.
  $line_item = _commerce_related_lines_retrieve_line($order, $product_line_item, $component_name);

  // Set type and (optional) product reference.
  _commerce_related_lines_title($line_item, $product_line_item, $component_name);

  // Now divide the amount.
  module_load_include('inc', 'commerce_line_item', 'commerce_line_item.rules');
  commerce_line_item_unit_price_divide($line_item, $amount, $component_name, $round_mode);

  // Now merge it into the order.
  _commerce_related_lines_merge_line($order, $line_item);
}

/**
 * Rules action: set the related line item price.
 */
function commerce_related_lines_price_amount($order, $product_line_item, $amount, $component_name, $round_mode) {
  // See if an existing related line item already exists.
  $line_item = _commerce_related_lines_retrieve_line($order, $product_line_item, $component_name);

  // Wrap the line item to easily set field information.
  $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);

  // Make sure there is data in commerce_unit_price.
  if (!isset($line_item->commerce_unit_price)) {
    $line_item_wrapper->commerce_unit_price = array();
  }

  // Set type and (optional) product reference.
  _commerce_related_lines_title($line_item, $product_line_item, $component_name);

  // Set the amount of the related line item unit price and add as a component.
  $unit_price = commerce_price_field_data_auto_creation();
  $unit_price['amount'] = $amount;

  // Add a price component for the related line item amount.
  $unit_price['data'] = commerce_price_component_add(
    $unit_price,
    $component_name,
    $unit_price,
    TRUE,
    FALSE
  );

  // Set the unit price.
  $line_item_wrapper->commerce_unit_price = $unit_price;

  // Now merge it into the order.
  _commerce_related_lines_merge_line($order, $line_item, FALSE);
}

/**
 * Check if an existing related line item exists, otherwise create a new stub.
 *
 * @param stdClass $order
 *   Commerce order object.
 * @param stdClass $product_line_item
 *   Commerce product line item.
 * @param string $price_type
 *   Price component machine name.
 *
 * @return array
 *   An existing related line item or a new stub.
 */
function _commerce_related_lines_retrieve_line($order, $product_line_item, $price_type) {
  $query = new EntityFieldQuery();

  $query->entityCondition('entity_type', 'commerce_line_item')
    ->entityCondition('bundle', 'related_lines')
    ->propertyCondition('order_id', $order->order_id)
    ->fieldCondition('commerce_related_lines_type', 'value', $price_type);

  // If a product is tied to the related line item, then add that to the query.
  if (!empty($product_line_item)) {
    $query->fieldCondition('commerce_product_line_item_ref', 'target_id', $product_line_item->line_item_id);
  }

  $result = $query->execute();

  $entities = array();
  if (!empty($result['commerce_line_item'])) {
    $entities = entity_load('commerce_line_item', array_keys($result['commerce_line_item']));
  }
  else {
    $entities[] = $line_item = entity_create('commerce_line_item', array(
      'type' => 'related_lines',
      'order_id' => $order->order_id,
      'quantity' => 1,
      'data' => array(),
    ));
  }

  return array_pop($entities);
}

/**
 * Set the line item price type and (optional) reference).
 *
 * @param stdClass $line_item
 *   Commerce related line item.
 * @param stdClass $product_line_item
 *   Commerce product line item.
 * @param string $price_type
 *   Price component machine name.
 */
function _commerce_related_lines_title($line_item, $product_line_item, $price_type) {
  $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);

  if (!empty($product_line_item)) {
    // Add the product reference value to the line item.
    $line_item_wrapper->commerce_product_line_item_ref = $product_line_item->line_item_id;
    // Set a SKU label.
    $line_item_wrapper->line_item_label = t('Related product: @label', array('@label' => $product_line_item->line_item_label));
  }

  // Set the type of price component for which this line is configured.
  $line_item_wrapper->commerce_related_lines_type = $price_type;
}

/**
 * Merge a related line item into the order.
 *
 * @param stdClass $order
 *   Commerce order object.
 * @param stdClass $line_item
 *   Commerce related line item.
 */
function _commerce_related_lines_merge_line($order, $line_item) {
  // Wrap the order & line item to easily set field information.
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  $line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);

  // Generate an array of properties and fields to compare.
  $comparison_properties = array('line_item_id');

  // Loop over each line item on the order.
  foreach ($order_wrapper->commerce_line_items as $delta => $matching_line_item_wrapper) {
    // Examine each of the comparison properties on the line item.
    foreach ($comparison_properties as $property) {
      // If the property is not present on either line item, bypass it.
      if (!isset($matching_line_item_wrapper->value()->{$property}) && !isset($line_item_wrapper->value()->{$property})) {
        continue;
      }

      // If any property does not match the same property on the incoming line
      // item or exists on one line item but not the other...
      if ((!isset($matching_line_item_wrapper->value()->{$property}) && isset($line_item_wrapper->value()->{$property})) ||
      (isset($matching_line_item_wrapper->value()->{$property}) && !isset($line_item_wrapper->value()->{$property})) ||
      $matching_line_item_wrapper->{$property}->raw() != $line_item_wrapper->{$property}->raw()) {
        // Continue the loop with the next line item.
        continue 2;
      }
    }

    // If every comparison line item matched, combine into this line item.
    $matching_line_item = $matching_line_item_wrapper->value();
    break;
  }

  // If no matching line item was found...
  if (empty($matching_line_item)) {
    // Save the incoming line item now so we get its ID.
    commerce_line_item_save($line_item);

    // Add it to the order's line item reference value.
    $order_wrapper->commerce_line_items[] = $line_item;

    // Save the updated order.
    commerce_order_save($order);
  }
  else {
    commerce_line_item_save($matching_line_item);

    // Clear the line item cache so the updated line item  will be available to
    // the ensuing load instead of the original line item as loaded above.
    entity_get_controller('commerce_line_item')->resetCache(array($matching_line_item->line_item_id));

    // Update the original line item variable.
    $line_item = $matching_line_item;
  }
}
