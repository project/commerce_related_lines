<?php
/**
 * @file
 * commerce_related_lines.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_related_lines_default_rules_configuration() {
  $rules = array();
  $rule = '{ "rules_cleanup_commerce_related_lines" : {
      "LABEL" : "Cleanup Commerce Related Lines",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "Commerce Related Lines" ],
      "REQUIRES" : [ "rules", "entity" ],
      "ON" : [ "commerce_line_item_delete" ],
      "IF" : [
        { "entity_is_of_bundle" : {
            "entity" : [ "commerce-line-item" ],
            "type" : "commerce_line_item",
            "bundle" : { "value" : { "product" : "product" } }
          }
        }
      ],
      "DO" : [
        { "entity_query" : {
            "USING" : {
              "type" : "commerce_line_item",
              "property" : "commerce_product_line_item_ref",
              "value" : [ "commerce-line-item:line-item-id" ],
              "limit" : "1"
            },
            "PROVIDE" : { "entity_fetched" : { "entity_fetched" : "Fetched entity" } }
          }
        },
        { "entity_delete" : { "data" : [ "entity-fetched:0" ] } }
      ]
    }
  }';
  $rules['rules_cleanup_commerce_related_lines'] = rules_import($rule);

  return $rules;
}
